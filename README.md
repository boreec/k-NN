# k-NN

Implementation of the [k-NN algorithm](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm) in python. The notebook may not render properly in the repository's web page, so I advise you to download it and run it locally (don't forget img folder).

Author : [Cyprien Borée](https://boreec.fr)

## Requirements

I used some python modules, some of them may be already embedded in your python installation :
- math
- mathplotlib
- numpy
- sklearn
- sys
