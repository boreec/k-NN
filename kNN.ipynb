{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# $k$-NN\n",
    "\n",
    "The algorithm of the $k$ nearest neighbours is the most simple one of the supervised classification algorithm. It is **non-parametric** and does not require a learning phase. The principle of the algorithm is the following :\n",
    "\n",
    "- We start from a learning set $D$ made of $n$ examples associated to a class : $(\\mathbf{x}_i,c_i)^n_{i=0}$.\n",
    "- Examples $\\mathbf{x}_i$ belong to the space of Attributes: $\\mathbb{X}$.\n",
    "- An attribute is a dimension of $\\mathbb{X}$.\n",
    "- Classes $c_i$ belong to a set $C$.\n",
    "- A new *unseen* example comes : $x_0$. We put this example is the space of Attributes :\n",
    "\n",
    "![](img/Exercice1_01.png)\n",
    "\n",
    "- We set the hyper-parameter $k = 3$. Using euclidean distance in $\\mathbb{X}$, we can find the 3 nearest neighbours of $x_0$ :\n",
    "\n",
    "![](img/Exercice1_02.png)\n",
    "\n",
    "- Among the 3 neighbours, we select the majority class. We predict that $x_0$ belongs to the *red* class.\n",
    "- $k$ is often selected as an odd number, to prevent *ex aequo* during majority vote."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Procedure\n",
    "1. load dataset **IRIS** :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import datasets\n",
    "data=datasets.load_iris()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Identify $n$ the amount of examples, $\\sharp C$ the amount of classes, $d = \\text{dim}(\\mathbb{X})$ the number of attributes and their natures."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "examples quantity (n):\t150\n",
      "classes amount (#C):\t3\n",
      "number of attributes d:\t4 (float64)\n"
     ]
    }
   ],
   "source": [
    "import numpy\n",
    "targets = data['target']\n",
    "samples_and_features = data['data']\n",
    "class_amount = len(numpy.unique(targets))\n",
    "print(\"examples quantity (n):\\t{}\".format(samples_and_features.shape[0]))\n",
    "print(\"classes amount (#C):\\t{}\".format(class_amount))\n",
    "print(\"number of attributes d:\\t{} ({})\".format(samples_and_features.shape[1], samples_and_features.dtype))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. We split the dataset into 2 subparts : $D_{app}$ (learning set) and $D_{test}$ (testing set). The learning set contains 50% of data and classes are distributed equally between the two sets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "D_test = {}\n",
    "D_app = {}\n",
    "\n",
    "tests_index = []\n",
    "app_index = []\n",
    "for i in range(150):\n",
    "    if i < 25 or 49 < i < 75 or 99 < i < 125:\n",
    "        tests_index.append(i)\n",
    "    else:\n",
    "        app_index.append(i)\n",
    "    \n",
    "for i in tests_index :\n",
    "    if targets[i] not in D_test :\n",
    "        D_test[targets[i]] = []\n",
    "    else :\n",
    "        D_test[targets[i]].append(samples_and_features[i])\n",
    "\n",
    "for i in app_index :\n",
    "    if targets[i] not in D_app :\n",
    "        D_app[targets[i]] = []\n",
    "    else :\n",
    "        D_app[targets[i]].append(samples_and_features[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Defining a function **knn**, taking as inputs an example $\\mathbf{x}$, learning set $D_{app}$ and hyper-parameter $k$. The output is the predicted class according to the $k$-NN alogithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import sys\n",
    "\n",
    "def euclidean_distance(p1 : numpy.ndarray, p2 : numpy.ndarray):\n",
    "    \"\"\"\n",
    "        Return the euclidean distance between two points if they have \n",
    "        the same number of dimensions.\n",
    "    \"\"\"\n",
    "    assert p1.shape == p2.shape, \"can't compute euclidean distance on two points with different dimensions.\"\n",
    "    result = 0\n",
    "    for i in range(p1.size):\n",
    "        result += (p1[i] - p2[i])**2\n",
    "    return math.sqrt(result)\n",
    "\n",
    "def compute_dataset_euclidean_distance(dataset : dict, x : numpy.array):\n",
    "    \"\"\"\n",
    "        Compute the euclidean distance for each points in a dataset and\n",
    "        a given point. The returned result is a list of tuples where each\n",
    "        tuples (x,y,z) corresponds to k,v in the dataset with the distance z.\n",
    "    \"\"\"\n",
    "    result = []\n",
    "    for key, values in dataset.items():\n",
    "        for v in values :\n",
    "            distance = euclidean_distance(v, x)\n",
    "            result.append((key, v, distance))\n",
    "    return result\n",
    "\n",
    "def knn(x, D_app, k):\n",
    "    distances = compute_dataset_euclidean_distance(D_app,x)\n",
    "    \n",
    "    closest_neighbours_classes = []\n",
    "    seen_neighbours = []\n",
    "    for i in range(k):\n",
    "        closest_neighbour = None\n",
    "        min_distance = sys.float_info.max\n",
    "        for d_i, d in enumerate(distances) :\n",
    "            if d[2] < min_distance and d_i not in seen_neighbours :\n",
    "                closest_neighbour = d_i\n",
    "                min_distance = d[2]\n",
    "        if closest_neighbour != None :\n",
    "            seen_neighbours.append(closest_neighbour)\n",
    "            closest_neighbours_classes.append(distances[closest_neighbour][0])\n",
    "\n",
    "    predicted_class = numpy.argmax(numpy.bincount(numpy.array(closest_neighbours_classes)))\n",
    "    return predicted_class\n",
    "\n",
    "    \n",
    "# how to use\n",
    "#knn(numpy.array([6,6,6,6]), D_app,40)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. Testing the function in a loop on every examples contained in $D_{test}$ and comparing *real class* and *predicted class*. We then deduct the good classification rate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9583333333333334\n"
     ]
    }
   ],
   "source": [
    "def compute_correct_classification_rate(D_app, D_test, k):\n",
    "    guess_correct = 0\n",
    "    total_guess = 0\n",
    "    for key, value in D_test.items():\n",
    "        for v in value :\n",
    "            predicted_class = knn(v, D_app, k)\n",
    "            if predicted_class == key :\n",
    "                guess_correct += 1\n",
    "            total_guess += 1\n",
    "    return guess_correct/total_guess\n",
    "\n",
    "print(compute_correct_classification_rate(D_app, D_test,3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6. We plot the classification rate depending on $k$ value (from 1 to 100 included) to find out the best value of $k$ to generalize."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYIAAAEJCAYAAACZjSCSAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8li6FKAAAgAElEQVR4nO3deXzU1b3/8dcnM5kkEwiLBIQAJgqioBA0oFZrtbYWrQq23oqtXbzt9dKfVm7t7a1d7Xbv7W17W7vYWmrVtrZatVbQUr299rpUrQZlkaVgWIQQlLDIDtk+vz9mEoaQZRLmm8nMvJ+PRx6Z7/ku8zks85lzzvd7jrk7IiKSu/LSHYCIiKSXEoGISI5TIhARyXFKBCIiOU6JQEQkxykRiIjkuMASgZndZWZbzWx5J/vNzH5oZjVmtszMzggqFhER6VyQLYJ7gBld7L8EGB//uR74aYCxiIhIJ8JBXdjdnzGz8i4OmQn8ymNPtP3NzAab2Uh339LVdYcNG+bl5V1dVkRE2nv55Ze3uXtpR/sCSwRJKAM2JWzXxsu6TATl5eUsWrQoyLhERLKOmb3e2b50DhZbB2UdzndhZteb2SIzW1RfXx9wWCIiuSWdiaAWGJOwPRqo6+hAd5/n7lXuXlVa2mHLRkREeimdiWAB8JH43UNnA7u6Gx8QEZHUC2yMwMzuAy4AhplZLXArkA/g7ncAC4FLgRpgP3BdULGIiEjngrxr6Jpu9jtwQ1DvLyIiydGTxSIiOU6JQEQkxykRJOmxZXW8vn1f0sevqNvFs6/pVlcR6f+UCJLwp1e3cONvF/Ojv9Qkdfybuw9y7Z0v8rG7q1m8cWfA0YmIHBslgm5s2XWAWx5+FYDqDTu6Pb6lxbn5gSUcbGyhdEABc+9fwt5DTUGHKSLSa0oEXWhucW7+3VIam1v40FljeX37frbuPtjlOXf+dR3P1WznK5dP5IfXTKV2535unb+ijyIWEem5dM411G+srd/LnoNHf2v/88o3eGHddr591WQmjBjIb17cyEsbdnDZ5FFtx+zY18DGHfsB2Lr7IN95YjXvmTSC2dPGYGbceOE4fviXGiaPHsSUMYMBGDW4kOEDC/umcgn2HmqiZuvebo8bPaSIYQMK+iAiEekPcj4R/P2N3cy47dlO97/39JH8w5mjaW5xopEQ1esPJwJ3Z/a8F1jz5uEP1xElBXzrfZMxi02ldNNF43m2Zhu3LjjcKijMz2PBjedx8oiBAdXqaHsONnL5j/7Khu37uz12QEGYRz91HhXDivsgMhFJt5xPBM/VbAfgh9dMZWDBkX8c4ZBx9onHYWaEQ8YZY4fw0obDg7+vbt7Fmjf3MucdJ3FWxVAATh89iCHFkYRr5PGbT5zFi+t3gENTi/P5h5dx032LeeSGcynMD/VBLeHWBSvYuGM/337/ZEoHdv5tv6G5hX97aBlz71/MQ3PeRiSs3kORbJfziaB6/Q5GDyniiimjuj12WvlQbntyDbsONDKoKJ9HFtcRCeXxyQtOYlBRfqfnRSNhLpwwvG07nDeF6+6p5tuPr+Yrl09MST26Mn/JZh5+ZTNzLxrPB6aN6fZ4d2fOva/w/f9dw+dmnBJ4fCKSXjn1da+hqeWIbXenesMOppcPTer8aRVDcIdXXt9JU3MLC5bW8c5ThneZBDpy4SnD+djbyrnrufU8tXprj87tqU079vOlPyznzBOG8Kl3jkvqnBmnjWT2tDHc8fRanl+7LdD4RCT9cqZF8PjyLXz2wWX8+eZ3cPyg2EDtum372L6vgWkVySWCqWOGkB8yXtqwg7w8Y9veQ8yaWtareG655BReWLud63/1MiVFXf81FIRD3H3dtCPGFPY3NHHVT19g656u72La39BMyIzbrq4kHEo+73/l8om8tH4HH7u7mpLCWHwjSgr5xUentf35iUh2yJlEMOH4EvYcauLRpXX80/knArFuIYh1+SSjKBLitLJBVK/fwZu7DlJSGObCU3q3PkJhfoiff6SKu55bT2NzS5fHPriolvte2sitl09qK/vzyjdZuWU3V0wZxcDCrv8aZ1aWMWZotEfxRSNh7vxoFb964XUam1tw4A+vbObmB5Zw78fPIi+vo3WFRCQT5UwiqBhWzJQxg/nD4s1tieClDTs4rjjCSaXJ3x0zvXwodz23npVbdjOzchQF4d4P9o49LspXr5jU7XE79jXw6NI6vnjpqW3f6v+weDNlg4u47erKwD6UTywdcER8U0YP4nO/f5V5z65jzjtOCuQ9RaTv5dQYwazKUazcsps1b+4BYk8KV5UPabvVMxnTK4bS2Ozsb2hmZmXvuoV6amZlGdv2NvDXmlh//ba9h3j2tW1cUTmqT7+Zf6BqDJeefjzffWI1y2rf6rP3FZFg5UyLAOCyyaP45h9X8cjizXzknHI27TjAR88p79E1qk4YihmMLClMepD5WF14SiklhWHmL6njggnDeWxpHc0tzpW9HJ/oLTPjP6+czOKNz/DJe1/h3HHHAVBSmM+N7xzH4GikmyvEBujvem4Dq9/Y3eH+S04byYWnDD+i7Pm123hz90GunDr62CshIkfJqURQOrCA88YNY/6SOiYcHxt4nZ7kQHGrQdF8rjpjNFPGDO6zb+MF4RDvnTyKRxZv5puzmnhkSR0TR5b06QNprQZF8/nRNVP53O+X8exrsRbK1j2H2LhjPz/78Jndtq4eWLSJbzy2kmEDCsgPHXnsrgONPL92O89OuLDtOu7OV+avYNOO/Vx06ghKCnt2h5aIdC+nEgHAlVPL+JffLeFnT6+jOBJi4siSHl/jO/8wJYDIujarchT3vbSRec+sY8mmt/jCpem7v7+qfChPfuaCtu2fP7OOf1+4ivte2sQHzxrb6Xnr6vfy1QUredtJx3U44PzwK7Xc/MBSXn59J1Xx1taKut1t02I8vvwNPlDV/XMQItIzOTVGAPDuiSMoyg+xcstuzjhhSI9uqUynaeVDKRtcxI/+8hpmcMWUvu0W6srHz6vg7eOH8fXHVnQ6l1FDUwtz719CQX4e3/tAxwPcF086nsL8PP6weHNb2fwlm8kPGWWDi3gkoVxEUifnWgTFBWHeM2kEjyyp67M+/lTIyzNmVo7iJ0+t5dxxx/Wre/nz8ozv/sMUZtz2DDfdt5ibLhp/1DFPr9nKq5t38bMPn9lp7AMKwlw88Xj++OoWbr18EqE8axsXmTiyhB/+5TXe2HWw27o3Nbfw3NrtHGhoPmpfQX4e548vJaTbX0Xa5FwigNjdL/OX1vH2k3v3DEC6vO+M0cx7Zl2/7B4ZUVLIt6+awj//ehFz7n25w2OuPXss75l0fJfXuXJqGQuW1vH0mnqikRBb9xxiVmUZE0eV8IMnX2PB0s1cf37Xt65+/bGV/OqF1zvd//2rp2jgWSSBuXu6Y+iRqqoqX7Ro0TFfZ9veQxk51fK2vYc4rjjSo1te+1LdWwd4a3/jUeX5IWPc8AHdxt3Y3MJZ//Ek55x4HEWREE8sf4PqL72LwvwQs25/joamFhbOfXun5z+56k0+/stFfPjsE7hm+tHjFdf/ehEVw4r59cfP6nnlRDKYmb3s7lUd7Qu0RWBmM4AfACHgTnf/Vrv9Q4C7gJOAg8A/uvvyIGNqlYlJAPp/3KMGFzFqcFGvz88P5XH55JHcV72JSCiPS08/vm2G1lmVo/jqoytZ/caetru+Em3dfZDPPrSMiSNL+NJlp3b4sN+syjJ+8lQNW/ccTMuaECL9UWAjpWYWAm4HLgEmAteYWfupNr8ALHH3ycBHiCUNyXGzppbR0NTC3kNNR8zldNmUUYTyjIcX19LY3HLET0NTC595cCn7G5r44TWVnT7xPWvqKFocHl26pa+qI9LvBdkimA7UuPs6ADO7H5gJrEw4ZiLwnwDu/nczKzezEe7+ZoBxST9XOWYw5cdFOdjYwtkVx7WVDxtQwNvHD+NnT6/jZ0+v6/Dcb846jXHDO3++YtzwgZxWVsL8JZv5+HkVKY9dJBMFmQjKgE0J27VA+47ZpcD7gL+a2XTgBGA0oESQw8yMH11zBk0tLUfdZvqVyyYyrfwNOhrbGjW4KKmnrWdVlvHNP66iZutexg0fkLK4RTJVkImgo1HB9v97vwX8wMyWAK8Ci4GjFg82s+uB6wHGju38gSXJHqePHtRh+YmlA7jhwuTWVejMFVNG8R8LVzF/yWY+c/GEY7qWSDYI8mmqWiDxPsfRQF3iAe6+292vc/dKYmMEpcD69hdy93nuXuXuVaWlmXXLp/Q/w0sKOXfcMB5ZsrnDloVIrgmyRVANjDezCmAzMBv4YOIBZjYY2O/uDcAngGfcvePZyERSaGZlGf/64FJe2biTM084/GChu/O1R1eytj72hLSZ8YnzKjg/w545EemJwFoE7t4E3Ag8AawCHnD3FWY2x8zmxA87FVhhZn8ndnfR3KDiEUl08aQR5Bk8vebIpThrdx7gnuc3ULvzAHsPNfG3tdt5bFldJ1cRyQ6BPkfg7guBhe3K7kh4/QJw9HwEIgErKczn1JElbavUtareENv+8QenMmnUIC787lPs72CqCpFskhkzrokEYFr5UBZv2klD0+GlQqs37GBgQZhTjo/NShuNhDqcs0gkmygRSM6aXjGUg40tLK/b1Vb20vodnFk+pG1SumgkxL6Go25kE8kqSgSSs6bFZ59t7R7avvcQa+v3tZUDRCNhtQgk6ykRSM4qHVhAxbDitnGB6g07gSNXrYu1CJQIJLspEUhOm1Y+hOoNO2lpcao37CASzmNywsNsahFILlAikJw2rXwouw408trWvVRv2EHl6MFHTFinMQLJBUoEktNau4GeWr2VFXW7j+gWAogWhHT7qGQ9JQLJaWOHRhk+sIC7n9tAc4szrX0iyA/T0NRCU3NLJ1cQyXxKBJLTzIxpFUN5Y/dB8gzOGDv4iP3FBbFuov2NahVI9lIikJw3PX676MRRJQwszD9iX1Eklgg0YCzZTIlAcl7rcwOJzw+0isYTwb5DGjCW7BXoXEMimeCU4wfyqXeOO2JZzFbRSOy/iAaMJZspEUjOy8uzTheoaW0RKBFINlPXkEgXDrcI1DUk2UuJQKQLahFILlAiEOlCscYIJAcoEYh04fDto+oakuylRCDShdYHyjQDqWQzJQKRLhSGNUYg2U+JQKQLeXlGUX6I/XqgTLKYEoFIN4oLQpprSLKaEoFIN4oiahFIdgs0EZjZDDNbbWY1ZnZLB/sHmdmjZrbUzFaY2XVBxiPSG8WRsMYIJKsFlgjMLATcDlwCTASuMbOJ7Q67AVjp7lOAC4D/NrNIUDGJ9EZRRIvTSHYLskUwHahx93Xu3gDcD8xsd4wDA83MgAHADkBtcOlXYi0C/bOU7BVkIigDNiVs18bLEv0YOBWoA14F5rq7loKSfkUtAsl2QSYC66DM222/B1gCjAIqgR+bWclRFzK73swWmdmi+vr61Ecq0oViJQLJckEmglpgTML2aGLf/BNdBzzsMTXAeuCU9hdy93nuXuXuVaWlpYEFLNKRIg0WS5YLMhFUA+PNrCI+ADwbWNDumI3ARQBmNgKYAKwLMCaRHou1CDRGINmr20RgZieb2ZNmtjy+PdnMvtTdee7eBNwIPAGsAh5w9xVmNsfM5sQP+wbwNjN7FXgS+Jy7b+ttZUSCEI2EONDYTEtL+55NkeyQzAplPwc+C/wMwN2XmdlvgW92d6K7LwQWtiu7I+F1HXBxTwIW6WtFkTDucLCpuW2hGpFskkzXUNTdX2pXpnay5IzWGUg1TiDZKplEsM3MTiJ+x4+ZXQVsCTQqkX6kKD+eCA4pEUh2SqadewMwDzjFzDYTu7PnQ4FGJdKPFBfEVylrVENYslMyicDd/V1mVgzkufseM6sIOjCR/qJI6xZLlkuma+j3AO6+z933xMseCi4kkf6lbd1idQ1Jluq0RWBmpwCTgEFm9r6EXSVAYdCBifQX0bYWgbqGJDt11TU0AbgMGAxcnlC+B/inIIMS6U+i6hqSLNdpInD3+cB8MzvH3V/ow5hE+pXWZweUCCRbJTNYvNjMbiDWTdTWJeTu/xhYVCL9SJG6hiTLJTNY/GvgeGIzhT5NbPK4PV2eIZJF1DUk2S6ZRDDO3b8M7HP3XwLvBU4PNiyR/iM/lEcklKdEIFkrmUTQGP/9lpmdBgwCygOLSKQfihZoBlLJXsmMEcwzsyHAl4hNIz0A+HKgUYn0M9F8LU4j2avLRGBmecBud98JPAOc2CdRifQz0QKtWyzZq8uuofj6wTf2USwi/VZUy1VKFktmjODPZvavZjbGzIa2/gQemUg/Eo2ENMWEZK1kxghanxe4IaHMUTeR5JBoJMzWPQfTHYZIILpNBO6umUYl56lFINksyMXrRbKGxggkmykRiCQhGtFdQ5K9lAhEkqAWgWSzZAaLMbMy4ITE4939maCCEulvopEQTS1OQ1MLkbC+P0l26TYRmNl/AVcDK4HWr0RO7AGz7s6dAfwACAF3uvu32u3/LIfXPw4DpwKl7r4j2QqI9IXDU1E3EQlH0hyNSGol0yKYBUxw90M9ubCZhYDbgXcDtUC1mS1w95Wtx7j7d4DvxI+/HPi0koD0R4kzkA6OpjkYkRRLpo27DsjvxbWnAzXuvs7dG4D7gZldHH8NcF8v3kckcNGCwy0CkWyTTItgP7DEzJ4E2loF7n5TN+eVAZsStmuBszo60MyiwAw6mc7CzK4HrgcYO3ZsEiGLpFY0X2sSSPZKJhEsiP/0lHVQ5p0ceznwXGfdQu4+D5gHUFVV1dk1RAITLYglgn16qEyyUDJPFv/SzCLAyfGi1e7e2NU5cbXAmITt0UBdJ8fORt1C0o+1DhYfaFTXkGSfbscIzOwC4DViA78/AdaY2flJXLsaGG9mFfFEMpsOWhZmNgh4BzC/B3GL9CktVynZLJmuof8GLnb31QBmdjKxb+9ndnWSuzeZ2Y3AE8RuH73L3VeY2Zz4/jvih14J/I+77+tlHUQC15YI1DUkWSiZRJDfmgQA3H2NmSV1F5G7LwQWtiu7o932PcA9yVxPJF0SnyMQyTbJJIJFZvYL4Nfx7Q8BLwcXkkj/09oi2KeuIclCySSCTxJbi+AmYncCPUNsrEAkZxSE88gzOKBEIFkombuGDgHfi/+I5CQzozgSZp+6hiQLdZoIzOwBd/+Amb1KB/f/u/vkQCMT6WeKIiG1CCQrddUimBv/fVlfBCLS3w0oCLP5rQNHlLk7X3pkOQ++XJuS9ygpDPPTa89kWrmWBZe+02kicPct8Zf/z90/l7gvPiPp544+SyR7zZpaxvf+vIb5SzYzs7IMgIderuU3L27kvaePZMzQY5+N7o+v1jH3vsX8ae75DIr2ZoovkZ4z965nbDCzV9z9jHZly9LVNVRVVeWLFi1Kx1tLjmtqbmH2vL+x+o09LJz7dppbnEt/+CyTRw/iN584m1BeR7Oq9MzSTW/x/p8+z3tOO54fXzMVs2O/pgiAmb3s7lUd7ev0yWIz+2R8fGCCmS1L+FkPLAsqWJH+KhzK4/tXVwLw6d8tYe79i8mPl6UiCQBMGTOYT7/7ZP64bAsPpai7SaQ7XY0R/Bb4E/CfwC0J5Xu0ZoDkqjFDo3zzytOYe/8SAH76oTMYOagope8x5x0n8cyaem5dsIK//H1rSq8NML1iKNedW5Hy60rm6mqMYBewi9g6AZjZcKAQGGBmA9x9Y9+EKNK/zKws47U39xIOGZecPjLl1w/lGd+/upLPPLCUtfV7U3rtA43N/Gn5G4woKeTSAGKXzJTMGMHlxJ4hGAVsJbZ28Sp3nxR8eEfTGIFI7zU2t3DVHS+wvn4vj//L+YwanNrWjPRfvRojSPBN4GxgjbtXABcBz6UwPhHpI/mhPH5wdSXNLc7NDyyhuUXLe0hyU0w0uvt2M8szszx3/7/47aMikoHKhxXztZmn8a8PLuXWBcupHDOky+OHDYjwjpNLdQdTFksmEbxlZgOIzTH0GzPbCug5e5EM9v4zyni+Zhv3/m0j9/6t++G+b8w6jQ+ffUIfRCbpkMwYQTFwgFg30oeAQcBv3H178OEdTWMEIqnh7mx+6wDdfATwxUeW8+K67Tz2qfMYP2Jg3wQnKdfVGEEyiaAC2OLuB+PbRcAId9+Q6kCToUQg0re27jnIJbc9S+nAAh654VwK80PpDkl64VgHix8EWhK2m+NlIpIDhg8s5NtXTebvb+zh24+v7v4EyTjJJIKwuze0bsRfR4ILSUT6m4tOHcFHzjmBu55bz/LNu9IdjqRYMomg3syuaN0ws5nAtuBCEpH+6DMXTyASztPUF1komUQwB/iCmW00s03EZh3952DDEpH+ZlBRPhedMpxHl9bR2NzS/QmSMbpNBO6+1t3PBiYCE939be5eE3xoItLfzJpaxvZ9Dfy1Rp0C2aSrFcqudfd7zezmduUAuLuWrhTJMRdMKGVQUT7zF2/mwgnD0x2OpEhXLYLWVTYGdvLTLTObYWarzazGzG7p5JgLzGyJma0ws6d7ELuI9LGCcIhLTx/JEyveZN8hPVeaLbp6svik+O+V7t7j20XNLATcDrwbqAWqzWyBu69MOGYw8BNghrtvjM9wKiL92JVTy7jvpY38eeWbzJpalu5wJAW6ahFcamb5wOd7ee3pQI27r4vfcno/MLPdMR8EHm6d0trdUz/5uoikVNUJQygbXMQfFm9OdyiSIl0lgseJ3SY62cx2J/zsMbPdSVy7DNiUsF0bL0t0MjDEzJ4ys5fN7CMdXcjMrjezRWa2qL6+Pom3FpGg5OUZMytH8deabdTvOZTucCQFOk0E7v5Zdx8E/NHdSxJ+Brp7SRLX7miqwvbzWYSBM4H3Au8BvmxmJ3cQyzx3r3L3qtLS0iTeWkSCdPmUUTS3OP+3Wo34bJDM7aPtu3OSVQuMSdgeDdR1cMzj7r7P3bcRm+F0Si/fT0T6yIQRAxkczad6vVatzQZdLV7/1/jvPQldQnt60DVUDYw3swoziwCzgQXtjpkPvN3MwmYWBc4CVvWuKiLSV/LyjKoThlK9QYkgG3S1ZvF58d+9mnfW3ZvM7EbgCSAE3OXuK8xsTnz/He6+ysweB5YRm9juTndf3pv3E5G+Nb1iCP+76k227jnI8IGF6Q5HjkG3C9OY2UlArbsfMrMLgMnAr9z9re7OdfeFwMJ2ZXe02/4O8J2eBC0i6TetfCgA1et38t7JI9McjRyLZOYa+j3QbGbjgF8AFcBvA41KRPq908oGUZQfUvdQFkgmEbS4exNwJXCbu38aUPoXyXH5oTymjh3MSxowznjJJIJGM7sG+CjwWLwsP7iQRCRTTCsfyqo3drP7YGO6Q5FjkEwiuA44B/h3d18fX7ry3mDDEpFMML1iKO7w8us70x2KHINkniNY6e43uft9ZjYEGOju3+qD2ESkn5s6djDhPNPzBBmu20QQn/6hxMyGAkuBu81MU1CLCNFImEllgzRgnOGS6Roa5O67gfcBd7v7mcC7gg1LRDLF9PIhLN20i4ONzekORXopqcXrzWwk8AEODxaLiACxAeOG5haW1WpR+0yVTCL4OrGng2vcvdrMTgReCzYsEckUE0fF5qBcV783zZFIb3X7ZHF8UZoHE7bXAe8PMigRyRwDCmIfI/sb1DWUqZKZYqIQ+DgwCWibUMTd/zHAuEQkQ0QjrYlAS1dmqmS6hn4NHE9svYCniU0nvSfIoEQkc0TCeYTzTC2CDJZMIhjn7l8G9rn7L4ktInN6sGGJSCaJRkJKBBksqSkm4r/fMrPTgEFAeWARiUjGiUbC6hrKYN2OEQDz4k8Uf5nYwjIDgK8EGpWIZJRoQYh9ahFkrGTuGroz/vJp4MRgwxGRTBSNhDigRJCxOk0EZnZzVye6u6aZEBEAovlh9h1S11Cm6qpF0KslKkUk90QLQuzY15DuMKSXulqz+Gt9GYiIZK5oJMSmHWoRZKpkZh/9pZkNTtgeYmZ3BRuWiGSSaCSsMYIMlszto5MTF6p3953A1OBCEpFME42E2K/ZRzNWMokgL377KADxdQmSue1URHJENBJm/yElgkyVTCL4b+B5M/uGmX0deB74djIXN7MZZrbazGrM7JYO9l9gZrvMbEn8R88niGSgaCREQ3MLjc0t6Q5FeiGZ5wh+ZWaLgHcCBrzP3Vd2d56ZhYDbgXcDtUC1mS3o4Nxn3f2ynocuIv1FNBICYjOQDipK5vul9CdJdfHEP7y7/fBvZzqxNQzWAZjZ/cDMXlxHRPq51hlIDzQ0M6goP83RSE8FmbrLgE0J27XxsvbOMbOlZvYnM5vU0YXM7HozW2Rmi+rr64OIVUSOQWuLYJ/mG8pIQSYC66DM222/Apzg7lOAHwGPdHQhd5/n7lXuXlVaWpriMEXkWLUmAt1CmpmCTAS1wJiE7dFAXeIB7r7b3ffGXy8E8s1sWIAxiUgAWruGNM1EZgoyEVQD482swswiwGxis5e2MbPjzczir6fH49keYEwiEoBoQXywWM8SZKTAngdw9yYzu5HYwvch4C53X2Fmc+L77wCuAj5pZk3AAWC2u7fvPhKRfk5dQ5kt0AfD4t09C9uV3ZHw+sfAj4OMQUSCV6yuoYymG35F5JgVtbYI1DWUkZQIROSYHW4RKBFkIiUCETlmhfl5mMEBPUeQkZQIROSYmRlF+Vq3OFMpEYhISkQjYfYrEWQkJQIRSYloJMR+dQ1lJCUCEUmJWCJQiyATKRGISEqoRZC5lAhEJCWKCzRGkKmUCEQkJYryQ5piIkMpEYhIShQXhLUeQYZSIhCRlCiKqEWQqZQIRCQlovkhTTGRoZQIRCQlogVhDjQ209KimeQzjRKBiKREVDOQZiwlAhFJieJ4ItAtpJlHiUBEUqIoPhW1HirLPEoEIpISahFkLiUCEUmJIiWCjKVEICIpUVygrqFMpUQgIilRlK8WQaZSIhCRlIi2dQ2pRZBpAk0EZjbDzFabWY2Z3dLFcdPMrNnMrgoyHhEJzuGuIbUIMk1gicDMQsDtwCXAROAaM5vYyXH/BTwRVCwiEry2wWJNM5FxgmwRTAdq3H2duzcA9wMzOzjuU8Dvga0BxiIiAYtqjCBjBZkIysGxd2kAAAd0SURBVIBNCdu18bI2ZlYGXAnc0dWFzOx6M1tkZovq6+tTHqiIHLtwKI9IOE9jBBkoyERgHZS1n43qNuBz7t7lVwh3n+fuVe5eVVpamrIARSS1irVucUYKB3jtWmBMwvZooK7dMVXA/WYGMAy41Mya3P2RAOMSkYBEI1quMhMFmQiqgfFmVgFsBmYDH0w8wN0rWl+b2T3AY0oCIplLC9hnpsASgbs3mdmNxO4GCgF3ufsKM5sT39/luICIZJ6ouoYyUpAtAtx9IbCwXVmHCcDdPxZkLCISvCK1CDKSniwWkZQp1hhBRlIiEJGUKVLXUEZSIhCRlIm1CNQ1lGmUCEQkZYoiIU0xkYGUCEQkZYoLQuxvbMa9/bOj0p8pEYhIykQjYZpbnENNLekORXpAiUBEUqZ1TYIDGjDOKEoEIpIybYvTNCoRZBIlAhFJmaJIfHGaQ7pzKJMoEYhIyhRHtCZBJlIiEJGUaV2lbJ+eJcgoSgQikjLF8a4hDRZnFiUCEUmZaFuLQIkgkygRiEjKRAtaWwTqGsokgU5DLSK5pXUB++/+zxrufHZ9mqPJPldPG8Mn3n5iyq+rRCAiKTM4ms/1559I7c796Q4lKw0bUBDIdZUIRCRlzIwvXHpqusOQHtIYgYhIjlMiEBHJcUoEIiI5TolARCTHKRGIiOQ4JQIRkRynRCAikuOUCEREcpxl2iLTZlYPvN7L04cB21IYTqbIxXrnYp0hN+udi3WGntf7BHcv7WhHxiWCY2Fmi9y9Kt1x9LVcrHcu1hlys965WGdIbb3VNSQikuOUCEREclyuJYJ56Q4gTXKx3rlYZ8jNeudinSGF9c6pMQIRETlarrUIRESknZxJBGY2w8xWm1mNmd2S7niCYGZjzOz/zGyVma0ws7nx8qFm9mczey3+e0i6Y001MwuZ2WIzeyy+nQt1HmxmD5nZ3+N/5+fkSL0/Hf/3vdzM7jOzwmyrt5ndZWZbzWx5QlmndTSzz8c/21ab2Xt6+n45kQjMLATcDlwCTASuMbOJ6Y0qEE3AZ9z9VOBs4IZ4PW8BnnT38cCT8e1sMxdYlbCdC3X+AfC4u58CTCFW/6yut5mVATcBVe5+GhACZpN99b4HmNGurMM6xv+PzwYmxc/5SfwzL2k5kQiA6UCNu69z9wbgfmBmmmNKOXff4u6vxF/vIfbBUEasrr+MH/ZLYFZ6IgyGmY0G3gvcmVCc7XUuAc4HfgHg7g3u/hZZXu+4MFBkZmEgCtSRZfV292eAHe2KO6vjTOB+dz/k7uuBGmKfeUnLlURQBmxK2K6Nl2UtMysHpgIvAiPcfQvEkgUwPH2RBeI24N+AloSybK/ziUA9cHe8S+xOMysmy+vt7puB7wIbgS3ALnf/H7K83nGd1fGYP99yJRFYB2VZe7uUmQ0Afg/8i7vvTnc8QTKzy4Ct7v5yumPpY2HgDOCn7j4V2Efmd4d0K94vPhOoAEYBxWZ2bXqjSrtj/nzLlURQC4xJ2B5NrDmZdcwsn1gS+I27PxwvftPMRsb3jwS2piu+AJwLXGFmG4h1+b3TzO4lu+sMsX/Tte7+Ynz7IWKJIdvr/S5gvbvXu3sj8DDwNrK/3tB5HY/58y1XEkE1MN7MKswsQmxgZUGaY0o5MzNifcar3P17CbsWAB+Nv/4oML+vYwuKu3/e3Ue7ezmxv9e/uPu1ZHGdAdz9DWCTmU2IF10ErCTL602sS+hsM4vG/71fRGwsLNvrDZ3XcQEw28wKzKwCGA+81KMru3tO/ACXAmuAtcAX0x1PQHU8j1iTcBmwJP5zKXAcsbsMXov/HpruWAOq/wXAY/HXWV9noBJYFP/7fgQYkiP1/hrwd2A58GugINvqDdxHbAykkdg3/o93VUfgi/HPttXAJT19Pz1ZLCKS43Kla0hERDqhRCAikuOUCEREcpwSgYhIjlMiEBHJcUoEIsSm5Eic6TFF17zHzK5K5TVFgqBEICKS45QIRNoxsxPjE7lNSyg71cxeStguN7Nl8ddfMbPq+Pz48+JPvLa/5gYzGxZ/XWVmT8VfF8fnnq+Ov2fWzYor/Z8SgUiC+JQNvweuc/fq1nJ3XwVEzOzEeNHVwAPx1z9292kemx+/CLisB2/5RWLTYkwDLgS+E59FVKTPKBGIHFZKbP6Wa919SQf7HwA+EH99NfC7+OsLzexFM3sVeCexBUKSdTFwi5ktAZ4CCoGxvYhdpNfC6Q5ApB/ZRWxe93OBFWZ2N7E1Herc/VJiH/wPmtnDgLv7a2ZWCPyE2IpZm8zsq8Q+zNtr4vAXr8T9Brzf3VcHUiORJKhFIHJYA7FVnz5iZh909+vcvTKeBHD3tUAz8GUOtwZaP9S3xdeB6OwuoQ3AmfHX708ofwL4VOu4gplNTVVlRJKlRCCSwN33Eevj/3QnA7e/A64lPj7gseUhfw68SmwG0OoOzoHYjJk/MLNniSWTVt8A8oFl8dtXv5GKeoj0hGYfFRHJcWoRiIjkOCUCEZEcp0QgIpLjlAhERHKcEoGISI5TIhARyXFKBCIiOU6JQEQkx/1/PeiiqiskywkAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "best k value to generalize : 5\n"
     ]
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def plot_kvalues():\n",
    "    ratios = []\n",
    "\n",
    "    for k in range(1,101):\n",
    "        ratios.append(compute_correct_classification_rate(D_app, D_test, k))\n",
    "\n",
    "    plt.plot(ratios)\n",
    "    plt.ylabel('classification rate')\n",
    "    plt.xlabel('k-value')\n",
    "    plt.show()\n",
    "    \n",
    "    print(\"best k value to generalize : {}\".format(ratios.index(max(ratios))))\n",
    "plot_kvalues()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
